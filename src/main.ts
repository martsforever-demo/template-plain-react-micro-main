import {MainApplication} from "@/micro.application";

/*
*  入口文件必须是`.ts`文件，而不能是`.tsx`；得让`ts-loader`来处理入口文件，否则node_modules中未编译的模块无法识别以及引入；
*/
export default MainApplication