import {designMainApplication} from "plain-micro-application/src/index";

export const MainApplication = designMainApplication({
    getPublicPath: () => __webpack_public_path__,
    setPublicPath: (path) => __webpack_public_path__ = path,
    baseUrl: 'http://localhost:3120/base.html',
    init: async (getConfig) => {
        const baseConfig = await getConfig({publicPath: __webpack_public_path__, data: {tag: '主应用给的数据'}})

        /*baseConfig.navigator.micro.registry({
            name: 'Vue3.0子应用',
            type: 'Vue3',
            pattern: /^\/?sub-vue\/.*!/,
            url: 'https://crm.linkcrm.cn/sub-vue3/',
        })*/

        console.log('初始化MAIN')

        baseConfig.publicAssets.initManagerApplication()

        return baseConfig
    },
})